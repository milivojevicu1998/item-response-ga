# Primena genetskih algortiama na Item-Responose teoriju na primeru testova

Kalibracija parametara pitanja po 3PL modelu i estimacija teta vrednosti ispitanika putem genetskih algoritama. Rezultati testa mogu biti importovani iz CSV fajlova. Moguce je eksportovati DAT fajlove za crtanje grafika kalibrisanih pitanja, informacione funkcjie testa i druge. Analiza dobijenih rezultata bi prikazala pitanja koja su prelaka ili preteška za datu grupu ispitanika, pitanja koja loše diskriminišu između teta vrednosti i time ne doprinose određivanju odgovarajuće ocene, pitanja za koje je velika šansa da ispitanik može nasumično izabrati tačan odgovor, kao i kvalitet celog testa na osnovu informacione funkcije testa.

 - ~~Kalibracija pitanja~~
 - ~~Estimacija teta vredonsti ispitanika~~
 - ~~Importovanje rezultata testa iz CSV fajlova~~
 - ~~Eksportovanje DAT fajlova za crtanje grafika~~
 - ~~Analiza rezultata~~
   - ~~Prelaka/preteška pitanja~~
   - ~~Pitanja koja loše diskriminišu izmedju teta vrednosti~~
   - ~~Pitanja za koja je velika šansa da ispitanik moze nasumično izabrati tačan odgovor~~
   - ~~Kvalitet celog testa na osnovu informacione funckije, odnosno greške, testa~~
