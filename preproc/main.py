#!python

import sys
import csv

import pandas

def main(xlsx_file : str, var : int, out_file : str) -> None:
    res = [None] * 1000
    for i in range(len(res)):
        res[i] = []
        for j in range(1000 * 3):
            res[i].append(-1)

    df = pandas.read_excel(xlsx_file)
    for index, row in df.iterrows():
        try:
            student_id = int(row["studentID"]) - 1
        except ValueError:
            continue

        """
        if not row["Q1"].startswith(var):
            continue
        """

        for col in ["Q1", "Q2", "Q3"]:
            arr = row[col].split(".")
            index = 1000 * (int(arr[0]) - 2) + 100 * (int(arr[1]) - 1) + int(arr[2]) - 1
            res[student_id][index] = int(row[col + "_response"])

    j = len(res[0]) - 1
    while j >= 0:
        num_of_answers = 0
        num_of_correct = 0
        for i in range(len(res)):
            if res[i][j] > 0:
                num_of_answers += 1
                num_of_correct += res[i][j]

        if num_of_answers == 0:
            for i in range(len(res)):
                del res[i][j]

        j -= 1

    i = len(res) - 1
    while i >= 0:
        num_of_answers = 0
        num_of_correct = 0
        for j in range(len(res[i])):
            if res[i][j] != -1:
                num_of_answers += 1
                num_of_correct += res[i][j]

        # remove examinees that have less then 3 answered questions or
        # all of their answers are correct or
        # all of their answers are incorrect
        if num_of_answers < 6: # or num_of_correct == num_of_answers or num_of_correct == 0:
            del res[i]
        i -= 1

    with open(out_file, "w") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(res)

if __name__ == "__main__":
    del sys.argv[0]
    if len(sys.argv) != 3:
        print("args: xlsx_file : str, var : int, out_file : str")

    main(*sys.argv)
