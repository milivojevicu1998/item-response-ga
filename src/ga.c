#include "ga.h"

int GA_MUTATION_TYPE = 2;
int GA_ADAPT_MUTATION = 1;
int GA_CROSSOVER_TYPE = 3;
size_t GA_NUM_OF_CROSSOVER_POINTS = 4;
int GA_STAGNATION_BUFFER_SIZE = 50;
size_t GA_CHROMOSOME_LENGTH = 15;
size_t GA_POPULATION_SIZE = 10;
int GA_CARRYOVER = 4;
double GA_MUTATION_RATE = 2.0;
size_t GA_DUMP_AMOUNT = 123;

double ETA_C = 2;

struct ga_candidate *population = NULL;
int generation = 0;
int max_generation = 0;
int decimals = (int) pow(10, GA_DECIMALS);
double mutation_rate = 0;
double (*fitness_function)(struct ga_candidate);
double (*mutation_adaptive_function)(double mutation_rate);
void (*crossover_function)(double gene_1, double gene_2, double *result_1, double *result_2);

int ga_comp_candidates(void *i1, void *i2) {
	double val1 = ((struct ga_candidate *) i1)->fitness;
	double val2 = ((struct ga_candidate *) i2)->fitness;
	return fpclassify(val1) != FP_NAN && val1 < val2;
}

void ga_gene_range(double *min, double *max, int index) {
	int type = index < (IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_PARAMTERES) ? index % IRT_NUM_OF_PARAMTERES : 0;
	if (type == 0) {
		*min = -3;
		*max =  3;
	} else if (type == 1) {
		*min = .3;
		*max =  3;
	} else if (type == 2) {
		*min =  0;
		*max = .5;
	}
}

double ga_rand_gene(int index) {
	double range_max = GA_RANGE_MAX, range_min = GA_RANGE_MIN;
	ga_gene_range(&range_min, &range_max, index);
	return (rand() % ((int) ((range_max - range_min) * decimals)) + (range_min * decimals)) / (float) decimals;
}

void ga_init_candidate(struct ga_candidate *candidate) {
	candidate->fitness = .0;

	for (size_t i = 0; i < GA_CHROMOSOME_LENGTH; i++) {
		candidate->chromosome[i] = ga_rand_gene(i);
	}

	for (int i = 0; i < IRT_NUM_OF_EXAMINEES * IRT_NUM_OF_QUESTIONS; i++) {
		candidate->fitness_matrix[i] = -1;
	}
}

void ga_init_population(double (*ff)(struct ga_candidate), size_t chromosome_length) {
	GA_CHROMOSOME_LENGTH = chromosome_length;

	ga_cleanup();
	population = malloc(sizeof (struct ga_candidate) * GA_POPULATION_SIZE);

	switch (GA_CROSSOVER_TYPE) {
		case 1:
			crossover_function = &ga_crossover_linear;
			break;
		case 2:
			crossover_function = &ga_crossover_blend;
			break;
		case 3:
			crossover_function = &ga_crossover_sbc;
			break;
		default:
			crossover_function = &ga_crossover_idk;
	}

	fitness_function = ff;
	for (size_t i = 0; i < GA_POPULATION_SIZE; i++) {
		population[i].fitness_matrix = malloc(sizeof (double) * IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_EXAMINEES);
		population[i].chromosome = malloc(sizeof (double) * GA_CHROMOSOME_LENGTH);
		ga_init_candidate(&population[i]);
		population[i].fitness = (*fitness_function)(population[i]);
	}

	sort(population, GA_POPULATION_SIZE, sizeof (struct ga_candidate), &ga_comp_candidates);
}

void ga_reinit_population(int keep) {
	for (size_t i = keep; i < GA_POPULATION_SIZE; i++) {
		ga_init_candidate(&population[i]);
		population[i].fitness = (*fitness_function)(population[i]);
	}

	sort(population, GA_POPULATION_SIZE, sizeof (struct ga_candidate), &ga_comp_candidates);
}

double ga_mutate(int candidate_index, int gene_index) {
	double original = population[candidate_index].chromosome[gene_index];
	double result;
	double min, max;
	ga_gene_range(&min, &max, gene_index);

	switch (GA_MUTATION_TYPE) {
		case 1: // non-uniform mutation
			double r = (double) rand() / (double) RAND_MAX;
			int tau = rand() % 2 ? -1 : 1;
			result = original + tau * (max - min) * (1 - pow(r, pow(1 - generation / max_generation, 1)));
			break;
		case 2: // normally distributed
			result = boxmuller(1, original);
			break;
		default:
			result = ga_rand_gene(gene_index);
	}

	if (result > max) {
		result = max;
	} else if (result < min) {
		result = min;
	}

	return result;
}

void ga_crossover_linear(double gene_1, double gene_2, double *result_1, double *result_2) {
	// not implemented, to slow because it generates 3 solutions and then picks the 2 with
	// the best fitness, causing 50% fitness calculations
	*result_1 = gene_1;
	*result_2 = gene_2;
}

void ga_crossover_blend(double gene_1, double gene_2, double *result_1, double *result_2) {
	double diff = 0.5 * fabs(gene_2 - gene_1);
	double range_min = gene_1 - diff;
	double range_max = gene_2 - diff;
	double fdiv = range_min - range_max;

	int div = (int) fdiv * 1000;
	if (div == 0) {
		if (fdiv < 0) div = -1;
		else div = 1;
	}

	*result_1 = (rand() % div) / 1000 + range_min;
	*result_2 = (rand() % div) / 1000 + range_min;
}

void ga_crossover_sbc(double gene_1, double gene_2, double *result_1, double *result_2) {
	double u = rand() % 1000000 / 1000000.0;
	double beta;

	if (u <= 0.5) {
		beta = pow(2 * u, 1 / ETA_C + 1);
	} else {
		beta = pow(2 * (1 - u), 1 / ETA_C + 1);
	}

	*result_1 = 0.5 * ((1 + beta) * gene_1 + (1 - beta) * gene_2);
	*result_2 = 0.5 * ((1 - beta) * gene_1 + (1 + beta) * gene_2);
}

void ga_crossover_idk(double gene_1, double gene_2, double *result_1, double *result_2) {
	double beta = rand() % 1000000 / 1000000.0;
	double diff = beta * (gene_2 - gene_1);
	*result_1 = gene_1 - diff;
	*result_2 = gene_2 + diff;
}

void ga_crossover(struct ga_candidate parent_1, struct ga_candidate parent_2, int offspring_index_1, int offspring_index_2) {
#pragma omp parallel for
	for (size_t i = 0; i < GA_CHROMOSOME_LENGTH; i++) {
		population[offspring_index_1].chromosome[i] = parent_1.chromosome[i];
		population[offspring_index_2].chromosome[i] = parent_2.chromosome[i];
	}

#pragma omp parallel for
	for (size_t i = 0; i < IRT_NUM_OF_EXAMINEES * IRT_NUM_OF_QUESTIONS; i++) {
		population[offspring_index_1].fitness_matrix[i] = parent_1.fitness_matrix[i];
		population[offspring_index_2].fitness_matrix[i] = parent_2.fitness_matrix[i];
	}

	for (size_t i = 0; i < GA_NUM_OF_CROSSOVER_POINTS; i++) {
		int crossover_point = rand() % GA_CHROMOSOME_LENGTH;

		if (crossover_point < IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_PARAMTERES) {
			int starting_index = (crossover_point / IRT_NUM_OF_PARAMTERES) * IRT_NUM_OF_EXAMINEES;
			for (int j = starting_index; j < starting_index + IRT_NUM_OF_EXAMINEES; j++) {
				population[offspring_index_1].fitness_matrix[j] = -1;
				population[offspring_index_2].fitness_matrix[j] = -1;
			}
		} else {
			int starting_index = crossover_point - IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_PARAMTERES;
			for (
				int j = starting_index;
				j < starting_index + IRT_NUM_OF_EXAMINEES * IRT_NUM_OF_QUESTIONS;
				j += IRT_NUM_OF_EXAMINEES
			) {
				population[offspring_index_1].fitness_matrix[j] = -1;
				population[offspring_index_2].fitness_matrix[j] = -1;
			}
		}

		if (mutation_rate > ((rand() % 100000) / 1000.0)) {
			population[offspring_index_1].chromosome[crossover_point] = ga_mutate(offspring_index_1, crossover_point);
			population[offspring_index_2].chromosome[crossover_point] = ga_mutate(offspring_index_2, crossover_point);
		} else {
			(*crossover_function)(
				parent_1.chromosome[crossover_point],
				parent_2.chromosome[crossover_point],
				&(population[offspring_index_1].chromosome[crossover_point]),
				&(population[offspring_index_2].chromosome[crossover_point])
			);
		}
	}

	population[offspring_index_1].fitness = (*fitness_function)(population[offspring_index_1]);
	population[offspring_index_2].fitness = (*fitness_function)(population[offspring_index_2]);
}

int ga_ranked_parent_choice() {
	int random = rand() % (GA_POPULATION_SIZE / 2 * (GA_POPULATION_SIZE + 1)) + 1;
	int index = 1;
	while (random > 0) {
		random -= index;
		index++;
	}
	return GA_POPULATION_SIZE - (--index);
}

double ga_avg_fitness() {
	double avg = .0;

#pragma omp parallel for reduction (+:avg)
	for (size_t i = 0; i < GA_POPULATION_SIZE; i++) {
		avg += population[i].fitness;
	}

	return avg / GA_POPULATION_SIZE;
}

double ga_std_deviation() {
	double avg = ga_avg_fitness();
	double std = .0;

#pragma omp parallel for reduction (+:std)
	for (size_t i = 0; i < GA_POPULATION_SIZE; i++) {
		double fit = population[i].fitness;
		std += pow(avg - fit, 2);
	}

	return sqrt(std);
}

int ga_already_in_population(size_t index) {
	for (size_t i = 0; i < index; i++) {
		if (population[i].fitness == population[index].fitness) {
			int same = 1;
			for (size_t j = 0; j < GA_CHROMOSOME_LENGTH; j++) {
				if (population[i].chromosome[j] != population[index].chromosome[j]) {
					same = 0;
					break;
				}
			}
			if (same) return 1;
		}
	}

	return 0;
}

void ga_next_generation() {
	if (GA_ADAPT_MUTATION) {
		int size = IRT_NUM_OF_EXAMINEES * IRT_NUM_OF_QUESTIONS;
		mutation_rate = GA_MUTATION_RATE / (50 * (ga_avg_fitness() - population[0].fitness) / size);
		mutation_rate = fmin(mutation_rate, GA_MUTATION_RATE * 10);
		mutation_rate = fmax(mutation_rate, GA_MUTATION_RATE / 10);
	} else {
		mutation_rate = GA_MUTATION_RATE;
	}

	struct ga_candidate old_population[GA_POPULATION_SIZE];
#pragma omp parallel for
	for (size_t i = 0; i < GA_POPULATION_SIZE; i++) {
		struct ga_candidate tmp;
		tmp.fitness = population[i].fitness;
		tmp.chromosome = malloc(sizeof (double) * GA_CHROMOSOME_LENGTH);
		for (size_t j = 0; j < GA_CHROMOSOME_LENGTH; j++) {
			tmp.chromosome[j] = population[i].chromosome[j];
		}
		tmp.fitness_matrix = malloc(sizeof (double) * IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_EXAMINEES);
		for (int j = 0; j < IRT_NUM_OF_EXAMINEES * IRT_NUM_OF_QUESTIONS; j++) {
			tmp.fitness_matrix[j] = population[i].fitness_matrix[j];
		}
		old_population[i] = tmp;
	}

	// #pragma omp parallel for
	for (size_t i = GA_CARRYOVER; i < GA_POPULATION_SIZE; i += 2) {
		int parent_1 = ga_ranked_parent_choice();
		int parent_2 = ga_ranked_parent_choice();
		while (parent_2 == parent_1) {
			parent_2 = ga_ranked_parent_choice();
		}
		ga_crossover(old_population[parent_1], old_population[parent_2], i, i + 1);
		/* too slow
		if (ga_already_in_population(i) || ga_already_in_population(i + 1)) {
			i -= 2;
		}
		*/
	}

#pragma omp parallel for
	for (size_t i = 0; i < GA_POPULATION_SIZE; i++) {
		free(old_population[i].chromosome);
		free(old_population[i].fitness_matrix);
	}

	sort(population, GA_POPULATION_SIZE, sizeof (struct ga_candidate), &ga_comp_candidates);

	/* scaling
	for (size_t i = 0; i < GA_POPULATION_SIZE; i++) {
		struct ga_candidate candidate = population[i];
		double b_min = -3.0, b_max = 3.0;
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			double b = candidate.chromosome[j * IRT_NUM_OF_PARAMTERES + 0];
			if (b < b_min) b_min = b;
			if (b > b_max) b_max = b;
		}

		if (b_min < -3.0 || b_max > 3.0) {
			for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
				double b = candidate.chromosome[j * IRT_NUM_OF_PARAMTERES + 0];
				candidate.chromosome[j * IRT_NUM_OF_PARAMTERES + 0] = (b - b_min) * (6/(b_max - b_min)) - 3;
			}
		}
	}
	*/


	generation++;
}

void ga_dump_candidate(FILE* stream, size_t index, struct ga_candidate candidate) {
	fprintf(stream, "%03ld: f=%7.2f c=", index, candidate.fitness);
	for (size_t j = 0; j < GA_CHROMOSOME_LENGTH; j++) {
		fprintf(stream, "%6.2f", candidate.chromosome[j]);
	}
	fprintf(stream, "\n");
	for (int j = 0; j < IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_EXAMINEES; j++) {
		fprintf(stream, "%6.2f", candidate.fitness_matrix[j]);
		if (!((j + 1) % IRT_NUM_OF_EXAMINEES)) {
			fprintf(stream, "\n");
		}
	}
	fprintf(stream, "\n");
}

void ga_dump_population(FILE* stream) {
	if (population == NULL) {
		printf("[WRN] GA population has not yet been initialized. Noting to dump.\n");
		return;
	}

	fprintf(stream, "GEN %03d:\n", generation);
	size_t limit = GA_POPULATION_SIZE < GA_DUMP_AMOUNT ? GA_POPULATION_SIZE : GA_DUMP_AMOUNT;
	for (size_t i = 0; i < limit; i++) {
		ga_dump_candidate(stream, i, population[i]);
	}
	fprintf(stream, "\n");
}

void ga_cleanup() {
	if (population != NULL) {
		for (size_t i = 0; i < GA_POPULATION_SIZE; i++) {
			free(population[i].chromosome);
		}
		free(population);
		population = NULL;
	}
}
