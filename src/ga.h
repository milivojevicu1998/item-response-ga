#ifndef DEF_GA
#define DEF_GA

// srand, rand
#include <stdlib.h>
// time
#include <time.h>
// pow, isnan
#include <math.h>
// printf
#include <stdio.h>
// sort
#include "utils.h"


#define GA_RANGE_MIN -3
#define GA_RANGE_MAX  3
#define GA_DECIMALS 2

extern int GA_MUTATION_TYPE;
extern int GA_ADAPT_MUTATION;
extern int GA_CROSSOVER_TYPE;
extern size_t GA_NUM_OF_CROSSOVER_POINTS;
extern int GA_STAGNATION_BUFFER_SIZE;
// calculated during initialization based on
// IRT_NUM_OF_QUESTIONS and IRT_NUM_OF_EXAMINEES
extern size_t GA_CHROMOSOME_LENGTH;
// settings
extern size_t GA_POPULATION_SIZE;
extern int GA_CARRYOVER;
extern double GA_MUTATION_RATE;
extern size_t GA_DUMP_AMOUNT;

// population candidate containing
// a chromosome wiht GA_CHROMOSOME_LENGTH genes and
// a fitness double for saving fitness calculation results
struct ga_candidate {
	double fitness;
	double *fitness_matrix;
	double *chromosome;
};
extern struct ga_candidate *population;

extern int max_generation;
// note: variables below are for internal use
// generation counter
extern int generation;
// gene precision indicator
extern int decimals;
// fitness function pointer to be set with population initialization
extern double (*fitness_function)(struct ga_candidate);


// candidate comparison based on saved fitness for utils/sort
int ga_comp_candidates(void *i1, void *i2);

// generate random double gene with irt/decimal based precision
double ga_rand_gene();

// initialize the ga_candidate structure with a random chromosome
void ga_init_candidate(struct ga_candidate *candidate);

// initialize the population by setting the fitness function,
// generating random candidates using ga_init_candidate, and
// calculating and saving their fitness values
void ga_init_population(double (*ff)(struct ga_candidate), size_t chromosome_length);
void ga_reinit_population(int keep);

// diff crossover functions
void ga_crossover_idk(double gene_1, double gene_2, double *result_1, double *result_2);
void ga_crossover_linear(double gene_1, double gene_2, double *result_1, double *result_2);
void ga_crossover_blend(double gene_1, double gene_2, double *result_1, double *result_2);
void ga_crossover_sbc(double gene_1, double gene_2, double *result_1, double *result_2);

// crossover two chromosomes using corssover point technique with mutation
void ga_crossover(struct ga_candidate parent_1, struct ga_candidate parent_2, int offsprint_index_1, int offsprint_index_2);

// random ranked choise of candidate in population as parent of crossover
int ga_ranked_parent_choice();

// returns avg fitness of the current population
double ga_avg_fitness();

// returns standard deviation of population fitness
double ga_std_deviation();

// returns non zero if at least one of the 2 generated candidates is already in the population
int ga_already_in_population(size_t i);

// generate next generation, using ga_crossover with carryover
void ga_next_generation();

// dump candidate
void ga_dump_candidate(FILE* stream, size_t index, struct ga_candidate candidate);

// dump population
void ga_dump_population(FILE *stream);

// cleanup alocated memory
void ga_cleanup();

#endif
