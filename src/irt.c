#include "irt.h"

struct irt_question *questions = NULL;
struct irt_examinee *examinees = NULL;

double irt_3pl_abc(double a, double b, double c, double theta) {
	double result = c + (1 - c) / (1 + pow(IRT_E, (-a * (theta - b))));
	if (result < 0) {
		printf("[ERR] irt_3pl_abc result = %5.2f\n", result);
	}
	return result;
}

double irt_3pl(struct irt_question question, double theta) {
	return irt_3pl_abc(question.a, question.b, question.c, theta);
}

double irt_question_error_abc(double a, double b, double c, int question_index) {
	double E = .0;

#pragma omp parallel for reduction (-:E)
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		double ans = examinees[i].answers[question_index];
		if (ans < 0) {
			continue;
		}

		double p = irt_3pl_abc(a, b, c, examinees[i].theta);
		if (ans == 1) {
			E -= log(p);
		} else if (ans == 0) {
			E -= log(1 - p);
		}
	}

	return E;
}

double irt_question_error(int question_index) {
	return irt_question_error_abc(
		questions[question_index].a, questions[question_index].b, questions[question_index].c, question_index
	);
}

double irt_question_rez_abc(double a, double b, double c, int question_index) {
	double E = .0;
#pragma omp parallel for reduction (-:E)
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		double ans = examinees[i].answers[question_index];
		if (ans < 0) {
			continue;
		}

		double p = irt_3pl_abc(a, b, c, examinees[i].theta);
		E += fabs(ans - p);
	}
	return E;
}

double irt_question_rez(int question_index) {
	return irt_question_rez_abc(
		questions[question_index].a, questions[question_index].b, questions[question_index].c, question_index
	);
}

double irt_question_fitness(struct ga_candidate candidate) {
	double fitness = .0;
#pragma omp parallel for reduction (+:fitness)
	for (size_t i = 0; i < GA_CHROMOSOME_LENGTH / IRT_NUM_OF_PARAMTERES; i++) {
		double a = 1.0, b, c = .0;
		switch (IRT_NUM_OF_PARAMTERES) {
			case 3:
				c = candidate.chromosome[i * IRT_NUM_OF_PARAMTERES + 2];
				[[fallthrough]];
			case 2:
				a = candidate.chromosome[i * IRT_NUM_OF_PARAMTERES + 1];
				[[fallthrough]];
			case 1:
				b = candidate.chromosome[i * IRT_NUM_OF_PARAMTERES + 0];
				break;
		}

		double E = irt_question_error_abc(a, b, c, i);
		
		if (isnan(E)) {
			printf("[ERR] Error function resturned NaN.\n");
			continue;
		}

		fitness += E;
	}
	return fitness;
}

double irt_examinee_error_abc(double theta, int examinee_index) {
	double E = .0;
#pragma omp parallel for reduction (-:E)
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		double ans = examinees[examinee_index].answers[i];
		if (ans < 0) {
			continue;
		}
		double p = irt_3pl(questions[i], theta);
		if (ans == 1) {
			E -= log(p);
		} else if (ans == 0) {
			E -= log(1 - p);
		} else {
			E -= ans * log(p) + (1 - ans) * log(1 - p);
		}
	}
	return E;
}

double irt_examinee_fitness(struct ga_candidate candidate) {
	double fitness = .0;
#pragma omp parallel for reduction (+:fitness)
	for (size_t i = 0; i < GA_CHROMOSOME_LENGTH; i++) {
		double theta = candidate.chromosome[i];

		double E = irt_examinee_error_abc(theta, i);
		if (isnan(E)) {
			printf("[ERR] Error function resturned NaN.\n");
			continue;
		}
		fitness += E;
	}
	return fitness;
}

double irt_both_fitness(struct ga_candidate candidate) {
	double fitness = .0;
	size_t start = IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_PARAMTERES;
#pragma omp parallel for reduction (+:fitness)
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		double a = 1.0, b, c = .0;
		switch (IRT_NUM_OF_PARAMTERES) {
			case 3:
				c = candidate.chromosome[i * IRT_NUM_OF_PARAMTERES + 2];
				[[fallthrough]];
			case 2:
				a = candidate.chromosome[i * IRT_NUM_OF_PARAMTERES + 1];
				[[fallthrough]];
			case 1:
				b = candidate.chromosome[i * IRT_NUM_OF_PARAMTERES + 0];
				break;
		}

		for (size_t j = start; j < GA_CHROMOSOME_LENGTH; j++) {
			size_t e = j - start;
			double theta = candidate.chromosome[j];

			double ans = examinees[e].answers[i];
			if (ans < 0) {
				continue;
			}

			int fitness_matrix_index = i * IRT_NUM_OF_EXAMINEES + e;
			double E = 0.0;
			if (candidate.fitness_matrix[fitness_matrix_index] == -1) {
				double p = irt_3pl_abc(a, b, c, theta);

				if (ans == 1) {
					E = -log(p);
				} else if (ans == 0) {
					E = -log(1 - p);
				}

				candidate.fitness_matrix[fitness_matrix_index] = E;

				if (isnan(E)) {
					printf("[ERR] Error function resturned NaN.\n");
					continue;
				}
			} else {
				E = candidate.fitness_matrix[fitness_matrix_index];
			}

			fitness += E;
		}
	}

	return fitness;
}

int irt_comp_questions(void *i1, void *i2) {
	return ((struct irt_question *) i1)->b > ((struct irt_question *) i2)->b;
}

int irt_comp_examinees(void *i1, void *i2) {
	return ((struct irt_examinee *) i1)->theta > ((struct irt_examinee *) i2)->theta;
}

double irt_bm_in_range(double min, double max) {
	double sigma = 1 / (6 / (max - min));
	double mu = (max + min) / 2;
	double res = boxmuller(sigma, mu);

	if (res < min) {
		res = min;
	} else if (res > max) {
		res = max;
	}

	return res;
}

double irt_bm_c() {
	double res = boxmuller(1, 0);

	res = fabs(res);
	res /= 6.0;
	res = fmin(0.3, res);
	res = fmax(0.0, res);

	return res;
}

double irt_bm_b() {
	double res = boxmuller(1, 0);

	res = fmin(+3.0, res);
	res = fmax(-3.0, res);

	return res;
}

double irt_bm_a() {
	double res = boxmuller(1, 0);

	if (res > 0) {
		res /= 1.5;
		res++;
		res = fmin(3.0, res);
	} else {
		res /= 10;
		res++;
		res = fmax(0.3, res);
	}

	return res;
}

void irt_gen() {
	irt_cleanup();

	// questions
	questions = malloc(sizeof (struct irt_question) * IRT_NUM_OF_QUESTIONS);
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		questions[i].id = i;
		questions[i].c = .0;
		questions[i].a = 1.0;
		switch (IRT_NUM_OF_PARAMTERES) {
			case 3:
				questions[i].c = irt_bm_c();
				[[fallthrough]];
			case 2:
				questions[i].a = irt_bm_a();
				[[fallthrough]];
			case 1:
				questions[i].b = irt_bm_b();
				break;
		}
		questions[i].num_of_answers = 0;
		questions[i].num_of_correct_answers = 0;
	}
	sort(questions, IRT_NUM_OF_QUESTIONS, sizeof (struct irt_question), &irt_comp_questions);

	// examinees
	examinees = malloc(sizeof (struct irt_examinee) * IRT_NUM_OF_EXAMINEES);
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		examinees[i].id = i;
		examinees[i].theta = irt_bm_in_range(-3, 3);
		examinees[i].answers = malloc(sizeof (double) * IRT_NUM_OF_QUESTIONS);
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			float ans = (rand() % 101 / 100.0) < (irt_3pl(questions[j], examinees[i].theta)) ? 1 : 0;
			examinees[i].answers[j] = ans;
			questions[j].num_of_answers++;
			if (ans >= 0) {
				questions[j].num_of_correct_answers += ans;
			}
		}
	}
	sort(examinees, IRT_NUM_OF_EXAMINEES, sizeof (struct irt_examinee), &irt_comp_examinees);

	irt_remove_extrems();
}

int irt_load(FILE *stream) {
	irt_cleanup();

	char c;
	int div = 0, count_questions = 1;
	IRT_NUM_OF_QUESTIONS = 1;
	IRT_NUM_OF_EXAMINEES = 0;
	while ((c = getc(stream)) != EOF) {
		div++;
		if (c == '\n') {
			IRT_NUM_OF_EXAMINEES++;
			count_questions = 0;
		} else if (c != ',') {
			continue;
		} else if (count_questions) {
			IRT_NUM_OF_QUESTIONS++;
		}
	}

	questions = malloc(sizeof (struct irt_question) * IRT_NUM_OF_QUESTIONS);
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		questions[i].id = i;
		questions[i].a = 1.0;
		questions[i].b = .0;
		questions[i].c = .0;
		questions[i].num_of_answers = 0;
		questions[i].num_of_correct_answers = .0;
	}
	examinees = malloc(sizeof (struct irt_examinee) * IRT_NUM_OF_EXAMINEES);
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		examinees[i].id = i;
		examinees[i].theta = .0;
		examinees[i].answers = malloc(sizeof (double) * IRT_NUM_OF_QUESTIONS);
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			examinees[i].answers[j] = -1;
		}
	}

	int i = 0, j = 0, count = 0, newline = 0, num_of_answered_questions = 0;
	char num[IRT_CSV_NUM_LENGTH];
	for (int i = 0; i < IRT_CSV_NUM_LENGTH - 1; i++) {
		num[i] = -1;
	}
	num[IRT_CSV_NUM_LENGTH - 1] = '\0';

	fseek(stream, 0, SEEK_SET);
	while (1) {
		c = getc(stream);

		switch (c) {
			case '\n':
				newline = 1;
				[[fallthrough]];
			case ' ':
			case ',':
				num[count] = '\0';
				float ans = atof(num);
				examinees[i].answers[j] = ans;
				if (ans >= 0) {
					num_of_answered_questions++;
					examinees[i].theta += ans;
					questions[j].num_of_answers++;
					questions[j].num_of_correct_answers += ans;
				}

				if (newline) {
					examinees[i].theta = (examinees[i].theta / (double) num_of_answered_questions) * 6 - 3;
					num_of_answered_questions = 0;
					i++;
					j = 0;
					newline = 0;
				} else {
					j++;
				}

				count = 0;
				[[fallthrough]];
			case '\r':
				continue;
			default:
				if (c != EOF && c != '-' && c != '.' && (c > '9' || c < '0')) {
					printf("NaN found '%d' @ %d,%d\n", c, i, j);
					irt_cleanup();
					return 1;
				}
		}

		if (c == EOF) {
			break;
		}

		num[count] = c;
		count++;
	}

	sort(examinees, IRT_NUM_OF_EXAMINEES, sizeof (struct irt_examinee), &irt_comp_examinees);

	irt_remove_extrems();

	return 0;
}

void irt_remove_extrems() {
	struct irt_question *tmp_questions = malloc(sizeof (struct irt_question) * IRT_NUM_OF_QUESTIONS);
	struct irt_examinee *tmp_examinees = malloc(sizeof (struct irt_examinee) * IRT_NUM_OF_EXAMINEES);
	int new_num_of_question = 0;

	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		tmp_examinees[i].answers = malloc(sizeof (double) * IRT_NUM_OF_QUESTIONS);
	}

	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		if (questions[i].num_of_correct_answers != questions[i].num_of_answers) {
			tmp_questions[new_num_of_question].id = questions[i].id;
			tmp_questions[new_num_of_question].a = questions[i].a;
			tmp_questions[new_num_of_question].b = questions[i].b;
			tmp_questions[new_num_of_question].c = questions[i].c;
			tmp_questions[new_num_of_question].num_of_answers = questions[i].num_of_answers;
			tmp_questions[new_num_of_question].num_of_correct_answers = questions[i].num_of_correct_answers;

			for (int j = 0; j < IRT_NUM_OF_EXAMINEES; j++) {
				tmp_examinees[j].answers[new_num_of_question] = examinees[j].answers[i];
			}

			new_num_of_question++;
		}
	}

	int new_num_of_examinees = 0;
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		int j;
		for (j = 1; j < new_num_of_question; j++) {
			if (tmp_examinees[i].answers[j] != tmp_examinees[i].answers[j - 1])
				break;
		}

		if (j != new_num_of_question) {
			tmp_examinees[new_num_of_examinees].id = examinees[i].id;
			tmp_examinees[new_num_of_examinees].theta = examinees[i].theta;
			tmp_examinees[new_num_of_examinees].answers = tmp_examinees[i].answers;

			new_num_of_examinees++;
		} else {
			free(tmp_examinees[i].answers);
			tmp_examinees[i].answers = NULL;
		}
	}

	IRT_NUM_OF_QUESTIONS = new_num_of_question;
	IRT_NUM_OF_EXAMINEES = new_num_of_examinees;

	irt_cleanup();

	questions = malloc(sizeof (struct irt_question) * IRT_NUM_OF_QUESTIONS);
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		questions[i].id = tmp_questions[i].id;
		questions[i].a = tmp_questions[i].a;
		questions[i].b = tmp_questions[i].b;
		questions[i].c = tmp_questions[i].c;
		questions[i].num_of_answers = 0;
		questions[i].num_of_correct_answers = 0;
	}

	examinees = malloc(sizeof (struct irt_examinee) * IRT_NUM_OF_EXAMINEES);
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		examinees[i].id = tmp_examinees[i].id;
		examinees[i].theta = tmp_examinees[i].theta;
		examinees[i].answers = tmp_examinees[i].answers;

		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			if (examinees[i].answers[j] >= 0) {
				questions[j].num_of_answers++;
				questions[j].num_of_correct_answers += examinees[i].answers[j];
			}
		}
	}

	free(tmp_questions);
	tmp_questions = NULL;

	free(tmp_examinees);
	tmp_examinees = NULL;
}

void irt_dump_questions(FILE *stream) {
	if (questions == NULL || examinees == NULL) {
		printf("[WRN] Questions or examinees and their answers have not been initialized. Nothing to dump.\n");
		return;
	}

	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		fprintf(stream, "%3d (%02d): %6.2f %6.2f %6.2f\n", i, questions[i].id, questions[i].a, questions[i].b, questions[i].c);
	}
}

void irt_dump_thetas(FILE *stream) {
	if (questions == NULL || examinees == NULL) {
		printf("[WRN] Questions or examinees and their answers have not been initialized. Nothing to dump.\n");
		return;
	}

	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		fprintf(stream, "%.2f\n", examinees[i].theta);
	}
}

void irt_dump_examinees(FILE *stream) {
	if (questions == NULL || examinees == NULL) {
		printf("[WRN] Questions or examinees and their answers have not been initialized. Nothing to dump.\n");
		return;
	}

	fprintf(stream, "\n");
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		fprintf(stream, "%3d (%03d): θ=%5.2f  ans=", i, examinees[i].id, examinees[i].theta);
		double total = 0;
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			float ans = examinees[i].answers[j];
			if (ans >= 0) {
				fprintf(stream, "%5.1f", ans);
				total += ans;
			} else {
				fprintf(stream, "   - ");
			}
		}
		fprintf(stream, "  ta=%4.2f\n", total);
	}

	if (questions != NULL) {
		fprintf(stream, "         num_of_answers=");
		for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
			fprintf(stream, "%5d", questions[i].num_of_answers);
		}
		fprintf(stream, "\n");
		fprintf(stream, "         num_of_correct=");
		for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
			fprintf(stream, "%5.1f", questions[i].num_of_correct_answers);
		}
		fprintf(stream, "\n");
	}

	fprintf(stream, "               question=");
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		fprintf(stream, "%5d", i);
	}
	
	fprintf(stream, "\n");

	fprintf(stream, "            question id=");
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		fprintf(stream, "%5d", questions[i].id);
	}

	fprintf(stream, "\n");
}

double irt_std_rez() {
	double result = 0.0;

	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			int ans = examinees[i].answers[j];
			double p = irt_3pl(questions[j], examinees[i].theta);

			result += pow((ans - p) / ((1 - p) * sqrt(p)), 2.0);
		}
	}

	result /= IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_EXAMINEES;

	return result;
}

double irt_std_zli() {
	double result = 0.0;

	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		double E = 0.0;
		double var_E = 0.0;

		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			double p = irt_3pl(questions[j], examinees[i].theta);

			E += p * log(p) + (1 - p) * log(1 - p);
			var_E += p * (1 - p) * pow(log(p / (1 - p)), 2.0);
		}

		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			int ans = examinees[i].answers[j];
			double p = irt_3pl(questions[j], examinees[i].theta);

			result += pow((ans * p + (1 - ans) * (1 - p) - E) / sqrt(var_E), 2.0);
		}
	}

	result /= IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_EXAMINEES;

	return result;
}

double irt_chi_sqr() {
	double result = 0.0;

	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			int ans = examinees[i].answers[j];
			double p = irt_3pl(questions[j], examinees[i].theta);

			result += pow(ans - p, 2.0) / p;
		}
	}

	result /= IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_EXAMINEES;

	return result;
}

void irt_print_stats(FILE *stream) {
	fprintf(stream,
		"std stats:\n * std_rez = %9.5f\n * std_zli = %9.5f\n * chi_sqr = %9.5f\n",
		irt_std_rez(), irt_std_zli(), irt_chi_sqr()
	);
}

void irt_plot_question_abc(double a, double b, double c, FILE *stream) {
	for (double x = -3; x < 3; x += 0.001) {
		fprintf(stream, "%.3f %.3f\n", x, irt_3pl_abc(a, b, c, x));
	}
}

void irt_plot_question(int question_index) {
	irt_plot_question_abc(questions[question_index].a, questions[question_index].b, questions[question_index].c, stdout);
}

void irt_cleanup() {
	if (questions != NULL) {
		free(questions);
		questions = NULL;
	}
	if (examinees != NULL) {
		for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
			free(examinees[i].answers);
		}
		free(examinees);
		examinees = NULL;
	}
}
