#ifndef DEF_IRT
#define DEF_IRT

// pow, log, fabs
#include <math.h>
// printf
#include <stdio.h>

// utility functions
#include "utils.h"
// ga_candidate structure
#include "ga.h"


#define IRT_E 2.718281828459045


// settings
static const int IRT_CSV_NUM_LENGTH = 16;

// question based on 3pl model
// + int for saving number of correct answers by examinees
struct irt_question {
	int id;
	double a, b, c;
	int num_of_answers;
	double num_of_correct_answers;
};
extern struct irt_question *questions;

// examinee theta and answers (0 - wrong, 1 - right)
struct irt_examinee {
	int id;
	double theta;
	double *answers;
};
extern struct irt_examinee *examinees;

//                              1
// P = c + (1 - c) * ----------------------
//                    1 + e^(-a * (θ - b))
double irt_3pl_abc(double a, double b, double c, double theta);
double irt_3pl(struct irt_question question, double theta);

// note for 2 functions below:
//  u = P(a, b, c, theta) or 1 - P(a, b, c, theta),
//  based on wheter the answers is right or wrong

// likelihood function: L *= u
// * has precision problems sometimes, but only after 14th decimal
// * error is slower by ~44%
// * returns -log(L), same result as E, better for readabilty
double irt_question_l2e_abc(double a, double b, double c, int question_index);
double irt_question_l2e(int question_index);

// using error function: E -= log(u), derived from L instead of L
// * L causes percision loss from 14th or 15th decimal in some cases,
//   while in others precision mathes even up to 40 decimals
double irt_question_error_abc(double a, double b, double c, int question_index);
double irt_question_error(int question_index);

// rezidual
double irt_question_rez_abc(double a, double b, double c, int question_index);
double irt_question_rez(int question_index);

// fitness function to be passed to the genetic algorithm
// for calibration question parameters
double irt_question_fitness(struct ga_candidate candidate);

// fitness function to be passed to the genetic algorithm
// for examinee ability estimation
double irt_examinee_fitness(struct ga_candidate candidate);

// TODO
double irt_both_fitness(struct ga_candidate candidate);

// compares questions based on b value for utils/sort
int irt_comp_questions(void *i1, void *i2);

// compares examinees based on theta value for utils/sort
int irt_comp_examinees(void *i1, void *i2);

// generates a normally distributed random number in the given range
double irt_bm_in_range(double min, double max);

// generate random questions and examinees,
// including random answers with probabilty based
// on the 3pl model
void irt_gen();

// load dataset from a stream
// format of the data should be a nxm matrix of 0s & 1s
// where n is number of questions, m is number of examinees
// and elements of the array represent correct (1) and incorect (0) answers
int irt_load(FILE *stream);

// remove examinees and questions with all correct or all incorrect answers and
void irt_remove_extrems();

// dump question params, examinees and answers
void irt_dump_questions(FILE *stream);
void irt_dump_thetas(FILE *stream);
void irt_dump_examinees(FILE *stream);

// stats
double irt_std_rez();
double irt_std_zli();
double irt_chi_sqr();

// print standardized stats
void irt_print_stats(FILE *stream);

// write question in a format for plotting
// "x y" per line, where x is in range [-3, 3]
// and y is calculated using irt_3pl with x as theta
void irt_plot_question_abc(double a, double b, double c, FILE *stream);
void irt_plot_question(int question_index);

// cleanup allocated memory
void irt_cleanup();

#endif
