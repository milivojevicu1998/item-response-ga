// print, f*
#include <stdio.h>
// strcmp
#include <string.h>
// floor
#include <math.h>
// errno
#include <errno.h>
// signal
#include <signal.h>
// time
#include <time.h>
// uint64_t
#include <stdint.h>

// genetic algorithm
#include "ga.h"
// utility functions
#include "utils.h"
// irt functions
#include "irt.h"

#define MAX_INSTRUCTION_LENGTH 64
#define MAX_INSTRUCTION_PARAMS 8
#define MODE_INTERACTIVE 0
#define MODE_FILE 1
#define PROMPT ">> "

// does cleanup and quits, used for premature exit
void quit(int);

int i_help();
int i_option();
int i_generate();
int i_seed();
int i_load();
int i_dump();
int i_analyze();
int i_both();
int i_plot();
int i_exit();

struct instruction {
	int num_of_parameters;
	char text_short;
	char text_long[10];
	int (*function)();
} instructions[] = {
	// print instructions
	{ 0, 'h', "help", &i_help },
	// set an option
	{ 2, 'o', "option", &i_option },
	// generate random dataset instead of reading from a file
	{ 0, 'g', "generate", &i_generate },
	// seed for the random dataset
	{ 1, 's', "seed", &i_seed },
	// load dataset from file
	{ 1, 'l', "load", &i_load },
	// dump loaded dataset
	{ 2, 'd', "dump", &i_dump },
	// plot dataset
	{ 2, 'a', "analyze", &i_analyze },
	// both calibrate and estimate n times for g generations
	{ 2, 'b', "both", &i_both },
	// plot a 3PL model
	{ 4, 'p', "plot", &i_plot },
	// exit
	{ 0, 'e', "exit", &i_exit },
};
size_t i_size = sizeof instructions / sizeof (struct instruction);

char *params[MAX_INSTRUCTION_PARAMS];

void interpret(char *text, char *name, int line, int mode) {
	char *token, *save, *delim = " \t", copy[MAX_INSTRUCTION_LENGTH];
	int index = -1, match = -1, fail = 0;

	strcpy(copy, text);

	token = strtok_r(copy, delim, &save);
	while (token != NULL) {
		if (index == -1) {
			if (strlen(token) == 1) {
				for (size_t i = 0; i < i_size; i++) {
					if ((*token) == instructions[i].text_short) {
						match = i;
						break;
					}
				}
			} else {
				for (size_t i = 0; i < i_size; i++) {
					if (strcmp(token, instructions[i].text_long) == 0) {
						match = i;
						break;
					}
				}
			}
		} else {
			params[index] = token;
		}
		token = strtok_r(NULL, delim, &save);
		index++;
	}

	if (match == -1) {
		printf("[CRT] No match found for instruction '%s'", copy);
		fail++;
	} else if (index != instructions[match].num_of_parameters) {
		printf("[CRT] Expected %d parameters, found %d for '%s'",
			instructions[match].num_of_parameters, index, instructions[match].text_long);
		fail++;
	} else {
		fail += (*(instructions[match].function))(params);
	}

	if (fail) {
		printf(" at:\n\t(%s:%d) '%s'\n", name, line, text);
		if (mode == MODE_FILE) quit(EXIT_FAILURE);
	}
}

FILE *file = NULL, *stream = NULL;

int main(int argc, char **argv) {
	char *name;
	char text[MAX_INSTRUCTION_LENGTH];
	int mode;

	signal(SIGINT, quit);

	if (argc >= 2) {
		mode = MODE_FILE;

		if (argc > 2) {
			printf("[WRN] More then 1 argument provided. Ignoring all arguments past first.\n");
		}

		name = argv[1];

		file = fopen(name, "r");
		if (file == NULL) {
			printf("[CRT] Could not open file %s\n", name);
			quit(EXIT_FAILURE);
		}
	} else {
		mode = MODE_INTERACTIVE;
		name = "shell";

		printf("\
    -- GA for IRT interactive mode --\n\
You can also pass a file with instructions\n\
instead of using interactive mode.\n\
\n\
'help' to view available instructions\n\
'exit' to quit the program\n\
\n\
Example:\n\
 1. Load the dataset from a file\n\
  >> load answers.csv\n\
 2. Run the GA for 1000 generations and save the output\n\
  >> calibrate 1000 fitness.dat result.dat\n\
\n\
");
		file = stdin;
	}

	if (mode == MODE_INTERACTIVE) printf(PROMPT);
	int c, i, line = 0, comment = 0;
	do {
		i = 0;
		while (1) {
			c = getc(file);
			if (c == '#') {
				if (i > 0) {
					text[i] = '\0';
					printf("[CRT] Comment starts before instruction ends at:\n\t(%s:%d) '%s#...'\n",
						name, line, text);
					quit(EXIT_FAILURE);
				}
				comment = 1;
			}

			if ((comment && c != '\n') || (i == 0 && c == ' ')) {
				continue;
			}

			if (c == ';' || c == EOF || c == '\n') {
				comment = 0;
				break;
			}

			text[i] = c;
			i++;

			if (i >= MAX_INSTRUCTION_LENGTH) {
				text[--i] = '\0';
				printf("[CRT] Exceeded maximum instruction length of %d at:\n\t(%s:%d) '%s...'\n",
					MAX_INSTRUCTION_LENGTH, name, line, text);
				quit(EXIT_FAILURE);
			}
		}

		if (i > 0) {
			text[i] = '\0';
			interpret(text, name, line, mode);
		}

		if (c == '\n') {
			if (mode == MODE_INTERACTIVE) printf(PROMPT);
			line++;
		}
	} while (c != EOF);

	if (fclose(file) == EOF) {
		printf("[CRT] Error occured while closing file %s. E%d", name, errno);
		quit(EXIT_FAILURE);
	}

	quit(EXIT_SUCCESS);
}

void quit(int code) {
	if (code != EXIT_SUCCESS) {
		if (file != NULL && fclose(file) == EOF) {
			printf("[CRT] Error occured while closing provided instruction file. E%d", errno);
		}
		if (stream != NULL && fclose(stream) == EOF) {
			printf("[CRT] Error occured while closing provided data file. E%d", errno);
		}
	}

	irt_cleanup();
	ga_cleanup();

	exit(code);
}

int i_help() {
	printf("\
Available instructions:\n\
	h/help\n\
		Print this message.\n\
	o/option [key] [value]\n\
		Set an option. List of available options can be see trough 'd/dump o' options dump instruction.\n\
		Note: Setting some options will reset the currentley loaded dataset (q, e) or the GA (l, p).\n\
	g/generate\n\
		Generate a random dataset. To use a set seed, use the 's/seed' instruction.\n\
	s/seed [n]\n\
		Set a seed to be used in 'g/generate' to [n]. To unset a seed (set a random seed), pass 0.\n\
	l/load [file]\n\
		Load a dataset from [file].\n\
	d/dump [key]\n\
		Avaliable keys: q, e, o, p.\n\
			q - Dump question parameters.\n\
			e - Dump loaded dataset.\n\
			o - Prints all the available options and their values.\n\
			p - Dump GA population.\n\
	a/analyze\n\
		Analyze the GA results.\n\
	b/both [g] [file]\n\
		Both calibrate question parameters and estimate theta values for [g] generations.\n\
	p/plot [a] [b] [c] [file]\n\
		Wirte data for plotting of the 3PL model with parameters [a], [b] and [c] to [file].\n\
	e/exit\n\
");
	return 0;
}

int i_option() {
	char key = *(params[0]);
	int value;
	double value_d;

	switch (key) {
		case 'm':
			value_d = atof(params[1]);
			break;
		default:
			value = atoi(params[1]);
	}

	switch (key) {
		case 'q':
			irt_cleanup();
			ga_cleanup();
			IRT_NUM_OF_QUESTIONS = value;
			break;
		case 'e':
			irt_cleanup();
			ga_cleanup();
			IRT_NUM_OF_EXAMINEES = value;
			break;
		case 'n':
			if (value > 3 || value < 0) {
				printf("[ERR] Number of model parameters must be in range [0, 3]");
				return 1;
			}
			IRT_NUM_OF_PARAMTERES = value;
			break;
		case 'p':
			ga_cleanup();
			GA_POPULATION_SIZE = value;
			break;
		case 'b':
			GA_CARRYOVER = value;
			break;
		case 'm':
			GA_MUTATION_RATE = value_d;
			break;
		case 's':
			GA_STAGNATION_BUFFER_SIZE = value;
			break;
		case 'r':
			GA_NUM_OF_CROSSOVER_POINTS = value;
			break;
		case 't':
			if (value > 2 || value < 0) {
				printf("[ERR] 3 types of mutation supported: 0 - random, 1 - uniform, 2 - normally distributed.");
				return 1;
			}
			GA_MUTATION_TYPE = value;
			break;
		case 'c':
			if (value > 3 || value < 0) {
				printf("[ERR] 4 types of crossover supported: 0 - idk, 1 - linear, 2 - blend, 3 - sbc.");
				return 1;
			}
			GA_CROSSOVER_TYPE = value;
			break;
		case 'a':
			GA_ADAPT_MUTATION = value;
			break;
		default:
			printf("[ERR] Unrecognized option key %c", key);
			return 1;
	}

	return 0;
}

int i_generate() {
	irt_gen();
	return 0;
}

int i_seed() {
	int seed = atoi(params[0]);

	if (seed == 0) {
		srand((unsigned int) time(NULL));
		printf("Seed randomized.\n");
	} else {
		srand((unsigned int) seed);
		printf("Seed set to %d.\n", seed);
	}

	return 0;
}

int i_load() {
	stream = fopen(params[0], "r");
	if (stream == NULL) {
		printf("[ERR] Failed to read file %s", params[3]);
		return 1;
	}

	int err = irt_load(stream);
	if (err) {
		printf("[ERR] Invalid file format provided");
		return 1;
	}

	if (fclose(stream) == EOF) {
		printf("[ERR] Failed to close provided data file %s", params[0]);
		return 1;
	}

	return err;
}

int i_dump() {
	char key = *(params[0]);
	int notstd = strcmp(params[1], "std");
	
	if (notstd) {
		stream = fopen(params[1], "w");
		if (stream == NULL) {
			printf("[ERR] Failed to read file %s", params[3]);
			return 1;
		}
	} else {
		stream = stdout;
	}

	switch (key) {
		case 'p':
			ga_dump_population(stream);
			break;
		case 'q':
			irt_dump_questions(stream);
			break;
		case 'e':
			irt_dump_examinees(stream);
			break;
		case 't':
			irt_dump_thetas(stream);
			break;
		case 'o':
			fprintf(stream, "\
IRT:\n\
* q / number of questions: %d\n\
* e / number of examinees: %d\n\
* n / number of model parameters: %d\n\
GA:\n\
* p / population size: %lu\n\
  b / carryover: %d\n\
  t / mutation type: %d\n\
  m / mutation rate: %.2f\n\
  a / mutation adapt: %d\n\
  c / crossover type: %d\n\
  r / no of crossoer points: %lu\n\
  s / stagnation buffer size: %d\n\
* Note: Marked options will reset the dataset/GA based on the category they fall under.\n\
",
IRT_NUM_OF_QUESTIONS,
IRT_NUM_OF_EXAMINEES,
IRT_NUM_OF_PARAMTERES,
GA_POPULATION_SIZE,
GA_CARRYOVER,
GA_MUTATION_TYPE,
GA_MUTATION_RATE,
GA_ADAPT_MUTATION,
GA_CROSSOVER_TYPE,
GA_NUM_OF_CROSSOVER_POINTS,
GA_STAGNATION_BUFFER_SIZE
);
			break;
		default:
			printf("[ERR] Unrecognized option key %c", key);
			return 1;
	}

	if (notstd) {
		if (fclose(stream) == EOF) {
			printf("[ERR] Failed to close provided data file %s", params[0]);
			return 1;
		}
	}

	return 0;
}

int i_analyze() {
	int remaining_examinees[IRT_NUM_OF_EXAMINEES];
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		remaining_examinees[i] = 0;
	}
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		remaining_examinees[examinees[i].id] = 1;
	}
	printf("Ispitanici ukolnjena zbog svih tačnih/netačnih odgovora: ");
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		if (remaining_examinees[i] == 0) {
			printf("%d, ", i);
		}
	}
	printf("\n");

	int remaining_questions[IRT_NUM_OF_QUESTIONS];
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		remaining_questions[i] = 0;
	}
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		remaining_questions[questions[i].id] = 1;
	}
	printf("Pitanja ukolnjena zbog svih tačnih/netačnih odgovora: ");
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		if (remaining_questions[i] == 0) {
			printf("%d, ", i);
		}
	}
	printf("\n");

	if (IRT_NUM_OF_PARAMTERES > 1) {
		printf("Pitanja sa ekstremnim vrednostima parametra a: ");
		for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
			if (questions[i].a > 1.75 || questions[i].a < 0.5) {
				printf("%d (%.2f), ", questions[i].id, questions[i].a);
			}
		}
		printf("\n");
	}

	if (IRT_NUM_OF_PARAMTERES > 2) {
		printf("Pitanja sa visokim vrednostima parametra c: ");
		for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
			if (questions[i].c > 0.2) {
				printf("%d (%.2f), ", questions[i].id, questions[i].c);
			}
		}
		printf("\n");
	}

	// init
	int size = 8;
	double bucket_avg[size][IRT_NUM_OF_QUESTIONS];
	int bucket_cnt[size][IRT_NUM_OF_QUESTIONS];
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			bucket_avg[i][j] = 0;
			bucket_cnt[i][j] = 0;
		}
	}

	// calc
	for (int i = 0; i < IRT_NUM_OF_EXAMINEES; i++) {
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			if (examinees[i].theta > 3 || examinees[i].theta < -3 || examinees[i].answers[j] < 0) {
				continue;
			}

			int index = (int) floor((examinees[i].theta + 3) * ((size - 1) / 6.0));

			int cnt = bucket_cnt[index][j];
			int avg = bucket_avg[index][j];
			double ans = examinees[i].answers[j];

			bucket_cnt[index][j]++;
			bucket_avg[index][j] = (cnt * avg + ans) / (double) (cnt + 1);
		}
	}

	// write
	stream = fopen(params[0], "w");
	if (stream == NULL) {
		printf("[ERR] Failed to read file %s", params[3]);
		return 1;
	}
	
	for (int i = 0; i < size; i++) {
		double bucket = i * (6 / (double) size) - 3;
		fprintf(stream, "%5.2f ", bucket);
		for (int j = 0; j < IRT_NUM_OF_QUESTIONS; j++) {
			if (bucket_cnt[i][j] == 0) {
				fprintf(stream, "%5.2f ", -1.0);
			} else {
				fprintf(stream, "%5.2f ", bucket_avg[i][j]);
			}
		}
		fprintf(stream, "\n");
	}

	if (fclose(stream) == EOF) {
		printf("[ERR] Failed to close provided data file %s", params[0]);
		return 1;
	}

	stream = fopen(params[1], "w");
	if (stream == NULL) {
		printf("[ERR] Failed to read file %s", params[3]);
		return 1;
	}

	fprintf(stream, "theta tcc tic\n");
	for (double x = -3; x < 3; x += 0.001) {
		double tcc = 0.0, tic = 0.0;
		for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
			double p = irt_3pl(questions[i], x);
			tcc += p;
			tic += p * (1-p);
		}
		tcc /= IRT_NUM_OF_QUESTIONS;
		tic /= IRT_NUM_OF_QUESTIONS;
		fprintf(stream, "%.3f %.3f %.3f\n", x, tcc, tic);
	}

	if (fclose(stream) == EOF) {
		printf("[ERR] Failed to close provided data file %s", params[3]);
		return 1;
	}

	return 0;
}

void decode() {
	for (int i = 0; i < IRT_NUM_OF_QUESTIONS; i++) {
		questions[i].a = 1.0;
		questions[i].c = 0.0;
		switch (IRT_NUM_OF_PARAMTERES) {
			case 3:
				questions[i].c = population[0].chromosome[i * IRT_NUM_OF_PARAMTERES + 2];
				[[fallthrough]];
			case 2:
				questions[i].a = population[0].chromosome[i * IRT_NUM_OF_PARAMTERES + 1];
				[[fallthrough]];
			case 1:
				questions[i].b = population[0].chromosome[i * IRT_NUM_OF_PARAMTERES + 0];
				break;
		}
	}
	sort(questions, IRT_NUM_OF_QUESTIONS, sizeof (struct irt_question), &irt_comp_questions);

	size_t start = IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_PARAMTERES;
	for (size_t j = start; j < GA_CHROMOSOME_LENGTH; j++) {
		size_t i = j - start;
		examinees[i].theta = population[0].chromosome[j];
	}
	sort(examinees, IRT_NUM_OF_EXAMINEES, sizeof (struct irt_examinee), &irt_comp_examinees);
}

int i_both() {
	max_generation = atoi(params[0]);

	stream = fopen(params[1], "w");
	if (stream == NULL) {
		printf("[ERR] Failed to read file %s", params[1]);
		return 1;
	}

	ga_cleanup();
	ga_init_population(&irt_both_fitness, IRT_NUM_OF_QUESTIONS * IRT_NUM_OF_PARAMTERES + IRT_NUM_OF_EXAMINEES);

	const int buffer_size = GA_STAGNATION_BUFFER_SIZE;
	int buffer_index = 0, buffer_equal = 0;
	float buffer[buffer_size];
	for (int i = 0; i < buffer_size; i++) {
		buffer[i] = .0;
	}

	//time_t start_t, end_t;
	//time(&start_t);

	//clock_t start = clock();

	struct timespec start, end;
	clock_gettime(CLOCK_MONOTONIC_RAW, &start);

	int i = 0;
	fprintf(stream, "gen best worst avg div\n");
	while ((buffer_equal != 1 || i < buffer_size) && i < max_generation) {
		fprintf(stream, "%d %f %f %f %f\n",
			i,
			population[0].fitness,
			population[GA_POPULATION_SIZE - 1].fitness,
			ga_avg_fitness(),
			ga_std_deviation()
		);
		fflush(stream);
		buffer[buffer_index] = population[0].fitness;
		buffer_index++;
		if (buffer_index >= buffer_size) {
			buffer_index = 0;
		}
		buffer_equal = 1;
		for (int j = 0; j < buffer_size - 1; j++) {
			if (buffer[j] != buffer[j + 1])
				buffer_equal = 0;
		}
		printf("%5d: %f      \r", i, population[0].fitness);
		fflush(stdout);
		i++;
		ga_next_generation();
	}

	//time(&end_t);
	//printf("Done after %.2lf seconds.\n", difftime(end_t, start_t));

	//clock_t end = clock();
	//float seconds = (float)(end - start) / CLOCKS_PER_SEC;
	//printf("Done after %.2lf seconds.\n", seconds);

	clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;

	printf("\n");
	printf("Done after %.2lf seconds.\n", delta_us / 1000000.0);

	if (fclose(stream) == EOF) {
		printf("[ERR] Failed to close provided data file %s", params[1]);
		return 1;
	}

	decode();

	irt_print_stats(stdout);

	return 0;
}

int i_plot() {
	double a = atof(params[0]);
	double b = atof(params[1]);
	double c = atof(params[2]);

	stream = fopen(params[3], "w");
	if (stream == NULL) {
		printf("[ERR] Failed to read file %s", params[3]);
		return 1;
	}

	fprintf(stream, "theta icc iic\n");
	for (double x = -3; x < 3; x += 0.001) {
		double icc = 0.0, iic = 0.0;
		double p = irt_3pl_abc(a, b, c, x);
		icc += p;
		iic += p * (1-p);
		fprintf(stream, "%.3f %.3f %.3f\n", x, icc, iic);
	}
	
	if (fclose(stream) == EOF) {
		printf("[ERR] Failed to close provided data file %s", params[3]);
		return 1;
	}

	return 0;
}

int i_exit() {
	quit(EXIT_SUCCESS);
	return 0;
}

