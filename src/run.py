#!python

import os
import sys
from random import randint
from subprocess import Popen, PIPE


FILE_IS = '.tmp.is'
NUM_QUESTIONS = 3
NUM_COMBINATIONS = 10


def parse():
    if len(sys.argv) < 3:
        print('Usage: run param_number path_data [path_questions] [path_examinees]')
        print('\tMissing arguments, `run` and `param_number` are requered.')
        exit(1)

    if len(sys.argv) > 5:
        return sys.argv[1:5]

    return sys.argv[1:]


def main(param_number: int, path_data: str, path_questions: str = None, path_examinees: str = None):
    code = '' + \
        f'l {path_data};' + \
        f'o n {param_number};' + \
        'o s 100;' + \
        'b 10000 tmp;' + \
        'd t std;' + \
        'a placeholder placeholder;' + \
        'd q .questions.dat'

    param_number = int(param_number)

    with open(FILE_IS, 'w') as file:
        file.write(code)

    proc = Popen(['./out', FILE_IS], stdout=PIPE, universal_newlines=True)
    result = proc.stdout.readlines()

    os.remove(FILE_IS)

    index_lists = []
    for i in range(-1, 2 * -1 * param_number - 4, -2):
        if result[i].strip() != '':
            tmp = list([r.split('(')[0].strip() for r in result[i].split(', ')])[:-1]
            index_lists.append(list([float(x) for x in tmp]))
        else:
            index_lists.append([])

    questions = []
    if path_questions:
        with open(path_questions, 'r', encoding='UTF8') as file:
            questions = file.readlines()
    examinees = []
    if path_examinees:
        with open(path_examinees, 'r', encoding='UTF8') as file:
            examinees = file.readlines()

    thetas = index_lists[-1]
    bad_questions = index_lists[-2]
    extr_examinees = index_lists[-1]

    if param_number == 3:
        bad_questions.extend(index_lists[1])
    if param_number == 2:
        bad_questions.extend(index_lists[0])

    print('Pitanja koja treba ispraviti ili izbaciti iz skupa:')
    for bad_question in bad_questions:
        print('%d ' % (int(bad_question)), end='')
        if path_questions:
            print(questions[int(bad_question)], end='')

    print()
    print('Ocene ispitanika:')
    for theta, examine in zip(thetas, examinees):
        grade = 5
        if theta > 0:
            grade += int(round(theta * (5.0/3.0)))
        print('%2d %s' % (grade, examine.strip()))
    with open('.questions.dat', 'r', encoding='UTF8') as file:
        question_params = [line.strip().split(' ') for line in file.readlines()]
        for j in range(len(question_params)):
            question = question_params[j]
            asd = question.count('')
            for i in range(asd):
                question.remove('')
            question_params[j] = question[1]
        bad_questions.sort()
        bad_questions.reverse()
        for i in bad_questions:
            del question_params[int(i)]
            del questions[int(i)]
        question_params.sort()

    print()
    print('Kombinacije pitanja:')
    for i in range(NUM_COMBINATIONS):
        for j in range(NUM_QUESTIONS):
            print(j+1, questions[randint(int(len(question_params) / NUM_QUESTIONS) * j,
                int(len(question_params) / NUM_QUESTIONS) * (j+1) - 1)].strip())
        print()

    os.remove('.questions.dat')


if __name__ == '__main__':
    main(*parse())
