#!python

import os
import sys
from math import sqrt
from random import randint
from subprocess import Popen, PIPE

from tqdm import tqdm


FILE_IS = '.tmp.is'
FILE_OUT = '.tmp.out'
NUM_RUNS = 11
SETTINGS = [1]
"""
SETTINGS = [x for x in range(4, 110, 16)]
SETTINGS = [1, 2, 3]
for i in range(len(SETTINGS)):
    if SETTINGS[i] % 2 != 0:
        SETTINGS[i] += 1
"""
NAME = 'time parallel real time 2'


base_output_file_name = os.path.join(sys.argv[1], NAME.replace(' ', '_'))

seeds = []
if len(sys.argv) < 3:
    for i in range(NUM_RUNS):
        seeds.append(randint(1, NUM_RUNS ** 2))
else:
    with (open(sys.argv[2], 'r')) as seed_file:
        for line in seed_file.readlines():
            seeds.append(int(line))
with open(base_output_file_name + '.seeds', 'w') as seed_file:
    seed_file.write('\n'.join([str(x) for x in seeds]))

stats = {
    'TIM': [],
    'GEN': [],
    'LOG': [],
    'REZ': [],
    'ZLI': [],
    'CHI': [],
}

if len(sys.argv) < 2:
    print("args: output_directory")
    exit()

code_file = open(base_output_file_name + '.is', 'w')
output_file = open(base_output_file_name + '.csv', 'w')
output_file.write('setting, ' +
    'tim_avg, tim_med, tim_div, tim_min, tim_max, ' +
    'gen_avg, gen_med, gen_div, gen_min, gen_max, ' +
    'log_avg, log_med, log_div, log_min, log_max, ' +
    'rez_avg, rez_med, rez_div, rez_min, rez_max, ' +
    'zli_avg, zli_med, zli_div, zli_min, zli_max, ' +
    'chi_avg, chi_med, chi_div, chi_min, chi_max\n')

for setting in SETTINGS:
    print('%s @ %s' % (NAME.upper(), str(setting)))
    output_file.write(str(setting));
    for i in tqdm(range(NUM_RUNS)):
        code = f'o n 1; o e 400; o q 40; o s 100; o c 3; o r 4; o a 1; o t 2; o m 2; o p 32; ' + \
            f's {seeds[i]}; g; o n 1; b 100000 {FILE_OUT}\n'
        code_file.write(code)
        with open(FILE_IS, 'w') as file:
            file.write(code)

        proc = Popen(['./out', FILE_IS], stdout=PIPE, universal_newlines=True)
        result = proc.stdout.readlines()[-6:]

        for i, key in enumerate(stats):
            if i == 0:
                stats[key].append(float(result[1].split(' ')[2]))
            elif i == 1:
                stats[key].append(int(result[0].split(':')[0]))
            elif i == 2:
                stats[key].append(float(result[0].split(':')[1]))
            else:
                stats[key].append(float(result[i].split(' = ')[1]))

    for i, key in enumerate(stats):
        avg = sum([(x / NUM_RUNS) for x in stats[key]])
        med = sorted(stats[key], key=float)[NUM_RUNS//2]
        div = sqrt(sum([(((x - avg) ** 2) / NUM_RUNS) for x in stats[key]]))

        print('%s: AVG = %6.2f, MED = %6.2f, DIV = %6.2f, MIN = %6.2f, MAX = %6.2f' % \
            (key, avg, med, div, min(stats[key]), max(stats[key])))
        output_file.write(', {}, {}, {}, {}, {}'.format(avg, med, div, min(stats[key]), max(stats[key])))
        stats[key] = []

    output_file.write('\n')
    output_file.flush()

code_file.close()
output_file.close()

os.remove(FILE_IS)
os.remove(FILE_OUT)
