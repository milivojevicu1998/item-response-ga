#include "utils.h"

int IRT_NUM_OF_PARAMTERES = 1;
int IRT_NUM_OF_QUESTIONS = 5;
int IRT_NUM_OF_EXAMINEES = 20;

double boxmuller_second;
int boxmuller_has_second = 0;

double boxmuller(double sigma, double mu) {
	if (boxmuller_has_second) {
		boxmuller_has_second = 0;
		return boxmuller_second;
	}

	double u1 = rand() % 10000 / 10000.0;
	double u2 = rand() % 10000 / 10000.0;

	double mag = sigma * sqrt(-2 * log(u1));

	boxmuller_second = mag * sin(2 * M_PI * u2) + mu;
	boxmuller_has_second = 1;

	return mag * cos(2 * M_PI * u2) + mu;
}

void sort(void *list, size_t length, size_t item_size, int (*compare)(void *item_1, void *item_2)) {
	for (size_t i = 0; i < length - 1; i++) {
		size_t best = i;
		for (size_t j = i + 1; j < length; j++) {
			char *item_1 = (char *) (list + item_size * best), *item_2 = (char *) (list + item_size * j);
			if ((*compare)(item_2, item_1)) {
				best = j;
			}
		}
		if (best != i) {
			char buffer[item_size], *item_1 = (char *) (list + item_size * i), *item_2 = (char *) (list + item_size * best);
			memcpy(buffer, item_1, item_size);
			memcpy(item_1, item_2, item_size);
			memcpy(item_2, buffer, item_size);
		}
	}
}
