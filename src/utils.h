#ifndef DEF_UTILS
#define DEF_UTILS

// memcpy
#include <string.h>
// srand, rand, atoi, malloc, free
#include <stdlib.h>
// sqrt
#include <math.h>


extern int IRT_NUM_OF_QUESTIONS;
extern int IRT_NUM_OF_PARAMTERES;
extern int IRT_NUM_OF_EXAMINEES;


extern double boxmuller_second;
extern int boxmuller_has_second;

// normally distributed random number pair generator
double boxmuller(double sigma, double mu);

// generic sort
// * compare should return 0 if wanted condition is met, aka item_1 is greater then item_2 for descending sort
void sort(void *list, size_t length, size_t item_size, int (*compare)(void *item_1, void *item_2));

#endif
