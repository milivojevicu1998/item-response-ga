\babel@toc {serbian}{}
\contentsline {section}{\numberline {1}UVOD}{7}{section.1}%
\contentsline {section}{\numberline {2}TEORIJSKE OSNOVE}{8}{section.2}%
\contentsline {subsection}{\numberline {2.1}Probabilistička teorija testova}{8}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Karakteristična kriva stavke}{10}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Karakteristična kriva testa}{11}{subsubsection.2.1.2}%
\contentsline {subsubsection}{\numberline {2.1.3}Informacione krive}{11}{subsubsection.2.1.3}%
\contentsline {subsubsection}{\numberline {2.1.4}Vrste modela}{12}{subsubsection.2.1.4}%
\contentsline {subsubsection}{\numberline {2.1.5}Parametar težine}{13}{subsubsection.2.1.5}%
\contentsline {subsubsection}{\numberline {2.1.6}Parametar diskrimiativnosti}{14}{subsubsection.2.1.6}%
\contentsline {subsubsection}{\numberline {2.1.7}Parametri pogađanja i nepažljivosti}{15}{subsubsection.2.1.7}%
\contentsline {subsection}{\numberline {2.2}Genetski algoritmi}{17}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Terminologija, komponente i process}{17}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Adaptivni genetski algoritmi}{18}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Mikro genetski algoritmi}{18}{subsubsection.2.2.3}%
\contentsline {subsubsection}{\numberline {2.2.4}Kodiranje parametara}{18}{subsubsection.2.2.4}%
\contentsline {subsubsection}{\numberline {2.2.5}Tipovi ukrštanja}{19}{subsubsection.2.2.5}%
\contentsline {subsubsection}{\numberline {2.2.6}Tipovi mutacije}{20}{subsubsection.2.2.6}%
\contentsline {subsection}{\numberline {2.3}Generacija normalno distribuiranih vrednosti}{21}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Validacija rezultata}{21}{subsection.2.4}%
\contentsline {subsubsection}{\numberline {2.4.1}Standardizovani reziduali}{21}{subsubsection.2.4.1}%
\contentsline {subsubsection}{\numberline {2.4.2}Standardizovani ZL indeks}{22}{subsubsection.2.4.2}%
\contentsline {section}{\numberline {3}PRIMENA GENETSKIH ALGORITAMA}{24}{section.3}%
\contentsline {subsection}{\numberline {3.1}Implementacija}{24}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Jezgro}{24}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Dodatni programi}{25}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Generacija skupova podataka}{26}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Kodiranje}{27}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Fitnes funkcije}{27}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Reziduali}{28}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}Verodostojnost}{28}{subsubsection.3.4.2}%
\contentsline {subsection}{\numberline {3.5}Parametri genetskog algoritma}{30}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Veličina populacije}{30}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}Mutacija}{31}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Ukrštanje}{32}{subsubsection.3.5.3}%
\contentsline {subsection}{\numberline {3.6}Skaliranje sa veličinom skupa podataka}{34}{subsection.3.6}%
\contentsline {subsection}{\numberline {3.7}Modeli}{35}{subsection.3.7}%
\contentsline {subsection}{\numberline {3.8}Rezultati na realnom skupu podataka}{36}{subsection.3.8}%
\contentsline {section}{\numberline {4}ZAKLJUČAK}{38}{section.4}%
\contentsline {section}{LITERATURA}{39}{section.4}%
\contentsline {section}{DODACI}{41}{section.4}%
\contentsline {subsection}{\numberline {A}Verovatnoća}{41}{subsection.Alph0.1}%
\contentsline {subsubsection}{\numberline {A.1}3PL verovatnoća}{41}{subsubsection.Alph0.1.1}%
\contentsline {subsubsection}{\numberline {A.2}Logartiam verovatnoće}{41}{subsubsection.Alph0.1.2}%
\contentsline {subsection}{\numberline {B}Metrike}{41}{subsection.Alph0.2}%
\contentsline {subsubsection}{\numberline {B.1}Standardizovani reziduali}{41}{subsubsection.Alph0.2.1}%
\contentsline {subsubsection}{\numberline {B.2}Standardizovani ZL indeks}{41}{subsubsection.Alph0.2.2}%
\contentsline {subsection}{\numberline {C}Generacija podataka}{42}{subsection.Alph0.3}%
\contentsline {subsubsection}{\numberline {C.1}Generacija parametara pitanja}{42}{subsubsection.Alph0.3.1}%
\contentsline {subsubsection}{\numberline {C.2}Generacija odgovora i $\theta $ vrednosti ispitanika}{42}{subsubsection.Alph0.3.2}%
\contentsline {subsubsection}{\numberline {C.3}Normalna distribucija}{43}{subsubsection.Alph0.3.3}%
\contentsline {subsection}{\numberline {D}Ukrštanje}{43}{subsection.Alph0.4}%
\contentsline {subsubsection}{\numberline {D.1}Mešano ukrštanje}{43}{subsubsection.Alph0.4.1}%
\contentsline {subsubsection}{\numberline {D.2}SBC ukrštanje}{43}{subsubsection.Alph0.4.2}%
\contentsline {subsection}{\numberline {E}Mutacija}{44}{subsection.Alph0.5}%
\contentsline {subsubsection}{\numberline {E.1}Nasumična mutacija}{44}{subsubsection.Alph0.5.1}%
\contentsline {subsubsection}{\numberline {E.2}Normalno distibuirana mutacija}{44}{subsubsection.Alph0.5.2}%
\contentsline {subsubsection}{\numberline {E.3}Neravnomerna mutacija}{44}{subsubsection.Alph0.5.3}%
\contentsline {subsection}{\numberline {F}Invalidacija}{44}{subsection.Alph0.6}%
